from django.urls import path

from todos.views import todos_list
urlpatterns = [
    path("", todos_list, name="todo_list")
]
