from django.contrib import admin
from todos.models import TodoList, TodoItem


class Todoadmin(admin.ModelAdmin):
    pass


class itemadmin(admin.ModelAdmin):
    pass


admin.site.register(TodoList)
admin.site.register(TodoItem)
