from django.shortcuts import render
from todos.models import TodoItem


def todos_list(request):
    todos = TodoItem.objects.all()
    context = {
        "todos": todos
    }
    return render(request, "todos/list.html", context)
